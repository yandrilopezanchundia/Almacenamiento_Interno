package lopezyandri.facci.prueba.almacenamientointernoprueba;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    Button buttonEscribirMI,buttonLeerMI;
    TextView textViewMostrar;
    EditText editTextEscribir,editTextTitulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewMostrar = (TextView)findViewById(R.id.textViewMostrar);
        editTextEscribir = (EditText)findViewById(R.id.editTextEscribir);
        editTextTitulo = (EditText)findViewById(R.id.editTextEscribirTitulo);
        buttonEscribirMI = (Button)findViewById(R.id.buttonEscribirMI);
        buttonLeerMI = (Button)findViewById(R.id.buttonLeerMI);


        buttonLeerMI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            openFileInput(editTextTitulo.getText().toString())));

                    String texto = fin.readLine();
                    textViewMostrar.setText(texto);
                    fin.close();

                }
                catch (Exception ex)
                {
                    Log.e("Ficheros", "Error al leer fichero desde la memoria interna");

                }
            }
        });
        buttonEscribirMI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    OutputStreamWriter fout=
                            new OutputStreamWriter(
                                    openFileOutput(editTextTitulo.getText().toString(), Context.MODE_PRIVATE));

                    fout.write(editTextEscribir.getText().toString());
                    fout.close();
                    Toast.makeText(getApplicationContext(),"Se ingreso correctamente",Toast.LENGTH_LONG).show();
                }
                catch (Exception ex)
                {
                    Log.e("Ficheros", "Error al escribir fichero en la memoria interna");
                }
            }
        });
    }
}
